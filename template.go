package main

const mainTemplate = `
<!DOCTYPE html>
<html>
  <head>
    <title>Demo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  </head>
  <body style="font-family: Georgia, 'Times New Roman', Times, serif; color: #555;">
    <div class="container">
      <div class="page-header">
        <h1>Network Policy Demo Application</h1>
        <p class="lead blog-description">Connect Pods based on services from different namespaces</p>
      </div>
      <form class="form-inline" action="/" accept-charset="UTF-8" method="post">
        <h4 class="form-signin-heading">Please provide the following information</h4>
        <label class="sr-only" for="namespace"></label>
        <input type="text" name="namespace" id="namespace" value="" size="35" placeholder="Namespace of target service" class="form-control" required="required">
        <label class="sr-only" for="name"></label>
        <input type="text" name="name" id="name" value="" size="35" placeholder="Name of target service" class="form-control" required="required">
        <input type="submit" name="commit" value="Ping" class="btn btn-primary mb-2" data-disable-with="Ping">
      </form>
      <div class="response" style="margin-top: 20px;">
        {{if .Error }}
          <div class='alert alert-warning show'>
            <h5 class='alert-heading'><i class='fa fa-warning' style="margin-right: 5px;"></i>{{.Error}} </h5>
          </div>
        {{else if .Response }}
          <div class='alert alert-success show'>
            <h5 class='alert-heading'><i class='fa fa-check' style="margin-right: 5px;"></i> Succeded with {{.Response.Status}}</h5>
          </div>
        {{ end }}
      </div>
    </div>
  </body>
</html>
`
